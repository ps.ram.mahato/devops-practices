#!/bin/bash

commit_date=$(git log -1 --format=%cd --date=format:'%y%m%d')
commit_hash=$(git log -1 --format=%H)
short_hash=${commit_hash:0:8}
version="${commit_date}-${short_hash}"

echo "Updating application version to: ${version}"
mvn versions:set -DnewVersion="${version}"

