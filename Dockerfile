# Step 1: Choose a base image
FROM adoptopenjdk:11-jre-hotspot

#stpe 2: set the Environment variable
ARG COMMIT_HASH
ENV LAST_COMMIT_HASH=$COMMIT_HASH

# Step 3: Set up a non-root user
RUN adduser --system --group appuser
USER appuser
# Step 4: Set the working directory
WORKDIR /home/app

# Step 5: Copy application artifact
COPY ./target/assignment-$LAST_COMMIT_HASH.jar /home/app/assignment-$LAST_COMMIT_HASH.jar

# Set environment variables
ENV DATABASE_TYPE=h2
ENV JAVA_OPTS=""

# Step 6: Expose necessary ports
EXPOSE 8090

# Step 7: Define container entry point
CMD java -jar /home/app/assignment-$LAST_COMMIT_HASH.jar --database.type=$DATABASE_TYPE $JAVA_OPTS